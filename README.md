# soal-shift-sisop-modul-2-ita07-2022
**Kelompok ITA07**
1. Maulanal Fatihil A. M.   5027201031
2. Cherylene Trevina        5027201033
3. M. Hilmi Azis            5027201049

# Daftar Isi
[Nomor 1](#nomor-1)
---
[Penyelesaian Nomor 1](#penyelesaian-nomor-1)
- [1a](#1a)
- [1e](#1e)

[Nomor 2](#nomor-2)
---
[Penyelesaian Nomor 2](#penyelesaian-nomor-2)
- [2a](#2a)
  - [Screenshot output soal 2a](#screenshot-output-soal-2a)
- [2b](#2b)
  - [Screenshot output soal 2b](#screenshot-output-soal-2b)
- [2c dan 2d](#2c-dan-2d)
  - [Screenshot output soal 2c dan 2d](#screenshot-output-soal-2c-dan-2d)
- [2e](#2e)
  - [Screenshot output soal 2e](#screenshot-output-soal-2e)

[Nomor 3](#nomor-3)
---
[Penyelesaian Nomor 3](#penyelesaian-nomor-3)
- [3a](#3a)
  - [Screenshot output soal 3a](#screenshot-output-soal-3a)
- [3b](#3b)
  - [Screenshot output soal 3b](#screenshot-output-soal-3b)
- [3c](#3c)
  - [Screenshot output soal 3c](#screenshot-output-soal-3c)
- [3d](#3d)
  - [Screenshot output soal 3d](#screenshot-output-soal-3d)
- [3e](#3e)
  - [Screenshot output soal 3e](#screenshot-output-soal-3e)

[Kendala Nomor 3](#kendala-nomor-3)

# Nomor 1
[Daftar isi](#daftar-isi)

a. Saat program pertama kali berjalan. Program akan mendownload [file characters](https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view) dan [file weapons](https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view), lalu program akan mengekstrak kedua file tersebut. Kemudian akan dibuat sebuah folder dengan nama `gacha_gacha` sebagai working directory.

b. Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Jumlah gacha **genap** -> gacha item **weapons**, jika bernilai **ganjil** -> gacha item **characters**. Setiap jumlah gacha = mod 10 -> buat file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Setiap jumlah gacha = mod 90 -> buat folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  
Intinya: 1 Folder -> 9 file(.txt). 1 file -> 10 hasil gacha. Setiap file isinya berbeda.

c. Format penamaan setiap file (.txt) nya adalah `{Hh:Mm:Ss}_gacha_{jumlah-gacha}`, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah `total_gacha_{jumlah-gacha}`, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

d. Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu gacha = 160 primogems. Primogems di awal di-define sebanyak **79000** primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu **name** dan **rarity**. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}`. Program akan selalu melakukan gacha hingga **primogems habis**.
Contoh : 157_characters_5_Albedo_53880

e.Proses gacha pertama kali: 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder `gacha_gacha` akan di zip dengan nama `not_safe_for_wibu` dengan dipassword `satuduatiga`, lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

> **Note**:
> - Menggunakan fork dan exec.
> - Tidak boleh menggunakan fungsi system(), mkdir(), dan rename().
> - Tidak boleh pake cron.
> - Semua poin dijalankan oleh **1** **script di latar belakang.** Cukup jalankan script 1x serta ubah time dan date untuk check hasilnya.

**Tips :**
- Gacha adalah proses untuk mendapatkan suatu item dengan cara melakukan randomize dari seluruh item yang ada.
- DIkarenakan file database memiliki format (.json). Silahkan gunakan library **<json-c/json.h>**, install dengan “**apt install libjson-c-dev”**, dan compile dengan “**gcc [nama_file] -l json-c -o [nama_file_output]”**.

# Penyelesaian Nomor 1
[Daftar isi](#daftar-isi)

Untuk source code dari soal nomor 1 dapat dilihat pada [nomor 1](/soal1/soal1.c)

Library yang digunakan pada penyelesaian nomor 1:
```c
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stddef.h>
#include <dirent.h>
```

## 1a

Pertama-tama untuk mendownload database weapon dan database character, kami menggunakan function `download1` dan `download2`. Source code yang kami gunakan:

```c
void download1()
{
  printf("Ini untuk download weapons");
  char downloadweapon[1000] = 
  {
    "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"
  };
  char weaponzip[1000] = 
  {
    "weapons.zip"
  };

    pid_t child_id;
    child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0)
    {
      char *argv[] = {"wget", "--no-check-certificate", downloadweapon, "-O", weaponzip, NULL};
      execv("/bin/wget", argv);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
  
}
```

```c
void download2()
{
  printf("Ini untuk download characters");
  char downloadcharacters[1000] = 
  {
    "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download"
  };
  char characterszip[1000] = 
  {
    "characters.zip"
  };

  
    pid_t child_id;
    child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0)
    {
      char *argv[] = {"wget", "--no-check-certificate", downloadcharacters, "-O", characterszip, NULL};
      execv("/bin/wget", argv);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
}
```
Untuk melakukan unzip pada kedua folder yang telah didownload, kami menggunakan program berikut:
```c
void unzipper() // buat ngeunzip character sama weapons
{
  pid_t child_id;
  child_id = fork();

    char *folder[] = {"unzip", "*.zip"};

    execv("/bin/unzip",folder);
}
```

Selanjutnya untuk membuat folder `gacha_gacha`, kami menggunakan function berikut:

```c
void makedirectory()
{
  DIR *gachafolder = opendir("gacha_gacha");
  pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
      char *argv_gachafolder[] = {"mkdir", "gacha_gacha", NULL};
      execv("/bin/mkdir", argv_gachafolder);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
}
```

## 1e
Untuk melakukan zip dan memberikan password, kami menggunakan program sebagai berikut:
```c
void zipper()
{
// $ zip -r <output_file> <folder_1> <folder_2> ... <folder_n> (ini cara ngezip)
printf("Start zipping files");
char zipfolder[1000] = {
  "gacha_gacha"
};
char zipoutput[1000] = {
  "not_safe_for_wibu.zip"
};
 pid_t child_id;
    child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0)
    {
      char *argv[] = {"zip", "-q", "-r", zipoutput, zipfolder, NULL};
      execv("/bin/zip", argv);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
}
```

Seluruh program dijadikan 1 ke dalam main, dengan urutan sebagai berikut:
```c
int main()
{
 download1(); 
 download2(); 
 makedirectory(); 
 unzipper(); 
 zipper(); 
}
```
# Nomor 2
[Daftar isi](#daftar-isi)

Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

a. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder `/home/[user]/shift2/drakor`. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam  `/drakor/romance`, jenis drama korea action akan disimpan dalam `/drakor/action`, dan seterusnya.

c. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: `/drakor/romance/start-up.png`.

d. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama `start-up;2020;romance_the-k2;2016;action.png` dipindah ke folder `/drakor/romance/start-up.png` dan `/drakor/action/the-k2.png`. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)

e. Di setiap folder kategori drama korea buatlah sebuah file "`data.txt`" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.

```
kategori : romance

nama : start-up
rilis  : tahun 2020

nama : twenty-one-twenty-five
rilis  : tahun 2022
```

**Note dan Ketentuan Soal:**
<ul>
<li>File zip berada dalam drive modul shift ini bernama drakor.zip</li>
<li>File yang penting hanyalah berbentuk .png</li>
<li>Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun rilis]:[kategori]. Jika terdapat lebih dari satu drama dalam poster, dipisahkan menggunakan underscore(_).</li>
<li>Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di bahasa C.</li>
<li>Gunakan bahasa pemrograman C (Tidak boleh yang lain).</li>
<li>Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).</li>
<li>[user] menyesuaikan nama user linux di os anda.</li>
</ul>

# Penyelesaian Nomor 2
[Daftar isi](#daftar-isi)

Untuk source code dari soal nomor 2 dapat dilihat pada [nomor 2](/soal2/soal2.c)

Pertama, kami melakukan inisiasi beberapa header, variabel dan fungsi :
```c
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
```

header yang kami gunakan adalah seperti terlihat diatas

```c
char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";
```

Variabel global yang diinisiasi adalah seperti diatas, variabel ini akan digunakan pada program untuk memudahkan kami dalam memproses sebuah perintah

```c
void processing(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0);
  }
}
```

Selanjutnya adalah fungsi `processing` yang memiliki 2 parameter, parameter pertama adalah variabel global key yang telah diinisiasi sebelumnya, parameter kedua adalah detail perintah yang akan dijalankan. Fungsi ini berguna untuk memproses sebuah perintah dengan fork dan execv.

## 2a
[Daftar isi](#daftar-isi)

```c
void start()
{
  char *make_dir[] = {"mkdir", "-p", "/home/kali/shift2/drakor/", NULL};
  processing(keyMkdir, make_dir);
  char *unzip[] = {"unzip", "-qo", "/home/kali/drakor.zip", "-d", "/home/kali/shift2/drakor", NULL};
  processing(keyUnzip, unzip);
  char *remove[] = {"rm", "-r", "/home/kali/shift2/drakor/coding/", "/home/kali/shift2/drakor/song/", "/home/kali/shift2/drakor/trash", NULL};
  processing(keyRemove, remove);
}
```

Fungsi `start()` diatas adalah fungsi yang digunkan untuk menyelesaikan no 2a. Di dalam fungsi tersebut terdapat 3 proses, yaitu membuat folder `home/kali/shift2/drakor` menggunakan variabel make_dir dan fungsi processing, selanjutnya melakukan unzip file yang berada di home dengan path `/home/kali/drakor.zip` dan hasilnya diletakkan di `/home/kali/shift2/drakor`, dan terakhir adalah menghapsu folder yang tidak diperlukan (coding, song, dan trash) menggunakan variabel remove dan fungsi processing

### Screenshot output soal 2a
Screenshot output untuk soal nomor 2:

![Output unzip](img/unzip.jpg)

## 2b
[Daftar isi](#daftar-isi)

```c
void category_folder()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/kali/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char path[100] = "/home/kali/shift2/drakor/";
          strcat(path, token3);
          char *createFolder3[] = {"mkdir", "-p", path, NULL};
          processing("/bin/mkdir", createFolder3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char path1[100] = "/home/kali/shift2/drakor/";
          char path2[100] = "/home/kali/shift2/drakor/";
          strcat(path1, token53);
          strcat(path2, token55);
          char *createFolder53[] = {"mkdir", "-p", path1, NULL};
          char *createFolder55[] = {"mkdir", "-p", path2, NULL};
          processing(keyMkdir, createFolder53);
          processing(keyMkdir, createFolder55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Untuk menyelesaikan fungsi ini, kami membuat sebuah fungsi yaitu `category_folder`.
```c
DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/kali/shift2/drakor/");

  if (dp != NULL)
  {
   ...
  }
  else
  {
    exit(0);
  }
```
Pertama, kami membuka direktori `/home/kali/shift2/drakor/` dan mengecek apakah direktori tersebut memiliki isi atau tidak
```c
    while ((ep = readdir(dp)))
    {
        ...
    }
    (void)closedir(dp);
```
Setelah itu, kami membaca setiap isi dari folder tersebut dengan perulangan while, setelah semua file terbaca, direktori yang tadi dibuka akan ditutup
```c
    if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char path[100] = "/home/kali/shift2/drakor/";
          strcat(path, token3);
          char *createFolder3[] = {"mkdir", "-p", path, NULL};
          processing("/bin/mkdir", createFolder3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char path1[100] = "/home/kali/shift2/drakor/";
          char path2[100] = "/home/kali/shift2/drakor/";
          strcat(path1, token53);
          strcat(path2, token55);
          char *createFolder53[] = {"mkdir", "-p", path1, NULL};
          char *createFolder55[] = {"mkdir", "-p", path2, NULL};
          processing(keyMkdir, createFolder53);
          processing(keyMkdir, createFolder55);
        }
     }
```
program diatas berfungsi untuk membuat folder dengan membaca setiap category dari nama file yang ada dalam folder drakor dan membuat direktori baru sesuai hasilnya

### Screenshot output soal 2b
Output 2b:

![Output result](img/make_cat.jpg)

## 2c dan 2d

[Daftar isi](#daftar-isi)

```c
void move_file()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/kali/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        strcpy(fileName, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char path[100] = "/home/kali/shift2/drakor/";
          char dest[100];
          char file_path[100];

          // copy the path
          strcpy(dest, path);
          strcpy(file_path, path);

          // define dest
          strcat(dest, token3);
          strcat(dest, "/");

          // define file sources
          strcat(file_path, fileName);

          char *moveFile3[] = {"mv", file_path, dest, NULL};
          processing(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char path[100] = "/home/kali/shift2/drakor/";
          char dest1[100];
          char dest2[100];
          char file_path1[100];
          char file_path2[100];

          // copy the path
          strcpy(dest1, path);
          strcpy(dest2, path);
          strcpy(file_path1, path);
          strcpy(file_path2, path);

          // define file sources
          strcat(file_path1, fileName);
          strcat(file_path2, fileName);

          // define dest1
          strcat(dest1, token53);
          strcat(dest1, "/");

          // define dest2
          strcat(dest2, token55);
          strcat(dest2, "/");

          char *copy_file[] = {"cp", file_path1, dest1, NULL};
          processing(keyCopy, copy_file);
          char *move_file[] = {"mv", file_path2, dest2, NULL};
          processing(keyMove, move_file);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Fungsi diatas adalah untuk memindahkan seluruh file yang ada sesuai category dan untuk file yang memiliki 2 kategori akan diduplikat dan dimasukkan ke dalam 2 folder sesuai kategorinya.

### Screenshot output soal 2c dan 2d
Berikut adalah screenshot output dari soal 2c dan 2d:

**Folder Drakor**
![Folder Drakor](img/folder%20drakor.jpg)

**Folder Action**
![Folder Action](img/folder%20action.jpg)

**Folder Comedy**
![Comedy](img/folder%20comedy.jpg)

## 2e
[Daftar isi](#daftar-isi)

Berikut ini adalah fungsi yang diperlukan fungsi utama untuk melakukan 2e.

```c
void sort(int arrTahun[5][1], char arrFilename[5][100], int n)
{
  int i, key, j;
  char kuy[100];
  for (i = 1; i < n; i++)
  {
    key = arrTahun[i][0];
    strcpy(kuy, arrFilename[i]);
    j = i - 1;

    while (j >= 0 && arrTahun[j][0] > key)
    {
      arrTahun[j + 1][0] = arrTahun[j][0];
      strcpy(arrFilename[j + 1], arrFilename[j]);
      j = j - 1;
    }
    arrTahun[j + 1][0] = key;
    strcpy(arrFilename[j + 1], kuy);
  }
}
```

Fungsi tersebut akan mengurutkan tahun terbit

```c
void rename_txt(char *whereFolder)
{
  DIR *dp;
  struct dirent *ep;
  char path[100] = "/home/kali/shift2/drakor/";
  strcat(path, whereFolder);

  dp = opendir(path);

  if (dp != NULL)
  {
    int arrTahun[5][1] = {};
    char arrFileName[5][100] = {};
    int idx = 0;

    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        char backupFilename[100];
        strcpy(fileName, ep->d_name);
        strcpy(backupFilename, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char path[100] = "/home/kali/shift2/drakor/";
          strcat(path, whereFolder);
          strcat(path, "/");

          char dest[100];
          char file_path[100];

          // copy the path
          strcpy(dest, path);
          strcpy(file_path, path);

          // define file sources
          strcat(file_path, fileName);

          // define dest
          strcat(dest, arr[0]);
          strcat(dest, ".png");

          arrTahun[idx][0] = atoi(arr[1]);
          strcpy(arrFileName[idx], arr[0]);
          idx++;

          char *moveFile3[] = {"mv", file_path, dest, NULL};
          processing(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char path[100] = "/home/kali/shift2/drakor/";
          strcat(path, whereFolder);
          strcat(path, "/");

          char dest[100];
          char file_path[100];

          // copy the path
          strcpy(dest, path);
          strcpy(file_path, path);

          // define file sources
          strcat(file_path, fileName);

          char *file53 = strtok(arr[2], "_");
          char *file55 = strtok(arr[4], ".");

          if (strcmp(file53, whereFolder) == 0)
          {
            // define dest
            strcat(dest, arr[0]);
            strcat(dest, ".png");

            arrTahun[idx][0] = atoi(arr[1]);
            strcpy(arrFileName[idx], arr[0]);
            idx++;
          }
          else if (strcmp(file55, whereFolder) == 0)
          {
            char *token52 = strtok(backupFilename, "_");
            char *token522 = strtok(token52, ";");

            // define dest
            strcat(dest, token522);
            strcat(dest, ".png");

            arrTahun[idx][0] = atoi(arr[3]);
            strcpy(arrFileName[idx], token522);
            idx++;
          }

          char *moveFile[] = {"mv", file_path, dest, NULL};
          processing(keyMove, moveFile);
        }
      }
    }

    strcat(path, "/data.txt");
    char *createTxt[] = {"touch", path, NULL};
    processing(keyTouch, createTxt);

    FILE *f = fopen(path, "w");
    if (f == NULL)
    {
      printf("Error when opening file %s \n", path);
      exit(1);
    }

    fprintf(f, "kategori: %s \n\n", whereFolder);

    sort(arrTahun, arrFileName, 5);

    for (int i = 0; i < 5; i++)
    {
      if (arrTahun[i][0] != 0)
      {
        fprintf(f, "nama: %s \n", arrFileName[i]);
        fprintf(f, "rilis: tahun %d \n\n", arrTahun[i][0]);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Fungsi diatas berfungsi untuk merename nama file, membuat file `data.txt` dan melakukan pengurutan dengan fungsi sebelumnya dan menambahkan hasilnya ke file `data.txt`

Fungsi dibawah ini adalah fungsi yang akan dipanggil untuk melakukan keseluruhan perintah
```c
void list()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/kali/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        rename_txt(ep->d_name);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
### Screenshot Output soal 2e
Berikut adalah screenshot output dari penyelesaian 2e

Folder Action:

![Folder action](img/isi%20folder%20action%20ada%20list.jpg)

Folder Action data.txt:

![data.txt di folder action](img/action%20data.txt.jpg)

Folder Comedy data.txt:

![data.txt di folder comedy](img/comedy%20data.txt.jpg)

Fungsi main yang digunakan untuk menjalankan seluruh program:
```c
int main()
{
  start();
  category_folder();
  move_file();
  list();
}
```

# Nomor 3
[Daftar isi](#daftar-isi)

Disediakan sebuah zip bernama `animal.zip`. Selanjutnya, dilakukan pengklasifikasian terhadap hewan-hewan dengan ketentuan: <br>
a. Program untuk membuat folder pada directory `/home/{user}/modul2/` dengan nama `darat` lalu 3 detik kemudian membuat folder `air`.<br>
b. Program untuk melakukan extract zip `animal.zip` pada `/home/{user}/modul2/`.<br>
c. Hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama file. Hewan darat -> `/home/{user}/modul2/darat`, 3 detik kemudian, hewan air -> `/home/{user}/modul2/air`. Untuk file yang tidak ada keterangan hewan air atau darat, dihapus.<br>
d. Menghapus semua burung yang terdapat pada `/home/{user}/modul2/darat`, ditandai dengan adanya `bird`. <br>
e. Membuat file `list.txt` di folder `/home/{user}/modul2/air`. Masukkan list nama hewan dari folder air ke dalam `list.txt` dengan format `UID_[UID file permission]_Nama File.[jpg/png]`. UID = user dari file, permission = permission file. Contoh format nama: conan_rwx_hewan.png

> Notes untuk nomor 3:
> * tidak boleh memakai system()
> * tidak boleh memakai function C mkdir() ataupun rename()
> * gunakan exec dan fork
> * direktori "." dan ".." tidak termasuk

# Penyelesaian Nomor 3
[Daftar isi](#daftar-isi)<br>

Untuk source code dari soal nomor 3 dapat dilihat pada [nomor 3](/soal3/soal3.c)

Library yang digunakan pada penyelesaian soal ini:
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>
```

## 3a
[Daftar isi](#daftar-isi)</br>

Pertama-tama, kami melakukan pendeklarasian terhadap variabel terlebih dahulu, yaitu:

```c
pid_t cid1, cid2, cid3, cid4, cid5, cid6, cid7, cid8, cid9;
int status;
```

Sesuai dengan konsep [wait x fork x exec](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2#wait-x-fork-x-exec), untuk membuat folder `darat`, kami melakukan fork pada child dengan id 1 atau `cid1`. Sedangkan untuk pembuatan folder `air`, kami menggunakan fork pada `cid2` dengan program sebagai berikut:
```c
    //bikin folder darat
    cid1 = fork();
    if(cid1 < 0) exit(EXIT_FAILURE);

    if(cid1 == 0){
        char *argv[] = {"mkdir", "-p", "/home/kali/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    } else 
        while((wait(&status)) > 0);
        
    //bikin folder air
    cid2 = fork();
    if(cid2 < 0) exit(EXIT_FAILURE);

    if(cid2 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/kali/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    } else while((wait(&status)) > 0);
```
* Program hanya akan berjalan ketika `cid == 0`
* Untuk pembuatan folder, digunakan `char *argv` yang merupakan variabel untuk menyimpan command membuat folder baru, yaitu `"mkdir", "-p", "/home/kali/modul2/*nama folder*, NULL`.
* Selanjutnya, command yang terletak pada `argv` akan dieksekusi dengan menggunakan `execv("/bin/mkdir", argv)`
* Untuk memberikan delay selama 3 detik setelah pembuatan folder `darat`, diberikan `sleep(3)`.

### Screenshot output soal 3a
Berikut adalah screenshot output ketika program tersebut dijalankan:

Folder Darat:

![folder darat](img/bikin%20folder%20darat.png)

Folder Air (setelah 3 detik):

![folder air](img/bikin%20folder%20air.png)

## 3b
[Daftar isi](#daftar-isi)

Untuk melakukan unzip dari file `animal.zip`, kami menggunakan fork pada cid3 dengan program berikut:
```c
    //unzip animal.zip
    cid3 = fork();

    if(cid3 < 0) exit(EXIT_FAILURE);

    if(cid3 == 0){
        char *argv[] = {"unzip", "-oq", "animal.zip", "-d", "/home/kali/modul2/", NULL};
        execv("/usr/bin/unzip", argv);
    } else while((wait(&status)) > 0);
```
* Command untuk unzip disimpan pada variabel `argv` dengan isi: `"unzip", "-oq", "/home/kali/Downloads/animal.zip", "-d", "/home/kali/modul2/", NULL`.
    * File animal.zip terletak pada folder `Downloads`.
    * `-o` digunakan untuk melakukan *overwrite* jika folder sudah ada di tempat tujuan
    * `q` digunakan agar program tidak mengeluarkan prompt apapun setelah selesai melakukan `extract` zip.
    * `-d` digunakan untuk menunjukkan directory tempat menyimpan hasil *unzip*
* Command `argv` dieksekusi dengan menggunakan `execv("usr/bin/unzip", argv)`

### Screenshot output soal 3b
Screenshot output nomor 3b:

![hasil unzip](img/hasil%20unzip.png)

## 3c
[Daftar isi](#daftar-isi)</br>

Untuk memindahkan file yang mengandung nama darat dan air ke folder masing-masing, digunakan program berikut:
```c
    //pindahin ke darat
    cid4 = fork();
    if(cid4 < 0) exit(EXIT_FAILURE);

    if(cid4 == 0){
        execl("/usr/bin/find", "find", "/home/kali/modul2/animal/", "-type", "f", "-name", "*darat*", "-exec", "mv", "-t", "/home/kali/modul2/darat", "{}", "+", (char *) NULL);
    } else while((wait(&status)) > 0);
                
    //pindahin ke air
    cid5 = fork();
    if(cid5 < 0) exit(EXIT_FAILURE);

    if(cid5 == 0){
        sleep(3);
        execl("/usr/bin/find", "find", "/home/kali/modul2/animal/", "-type", "f", "-name", "*air*", "-exec", "mv", "-t", "/home/kali/modul2/air/", "{}", "+", (char *) NULL);
    } else while(wait((&status)) > 0);
```
* Untuk memindahkan file, pertama dilakukan `find` terlebih dahulu, terhadap seluruh file yang mengandung `*darat*` dan `*air*`. Setelah itu, dilakukan `"-exec", "mv", "-t", "/home/kali/modul2/darat"` dan `"-exec", "mv", "-t", "/home/kali/modul2/air/"` untuk memindahkan file-file tersebut ke directory yang diinginkan.
* Diberikan `sleep(3)` pada pembuatan folder air untuk memberikan delay sebanyak 3 detik setelah pembuatan folder darat.

Selanjutnya, untuk menghapus file yang tersisa pada directory animal, digunakan program berikut:
```c
    //remove file in dir: animal
    cid6 = fork();

    if(cid6 < 0) exit(EXIT_FAILURE);

    if(cid6 == 0) 
        execl("/bin/sh", "sh", "-c", "rm -f /home/kali/modul2/animal/*", (char *) NULL);
    else while(wait((&status)) > 0);
```
* `rm` adalah perintah dalam linux untuk melakukan *remove*
* `-f` atau *force* digunakan untuk memaksa program agar dapat langsung *remove* tanpa mengeluarkan prompt
* `/home/kali/modul2/animal/*` berarti menghapus seluruh file yang berada pada directory animal.

### Screenshot output soal 3c
Folder air:

![folder air](img/isi%20air.png)

Folder darat:

![folder darat](img/isi%20darat.png)

Folder animal:

![folder animal](img/isi%20animal.png)

## 3d
[Daftar isi](#daftar-isi)</br>

Untuk menghapus file yang memiliki nama **bird** pada folder darat, digunakan program sebagai berikut:
```c
    //remove bird
    cid7 = fork();
    if(cid7 < 0) exit(EXIT_FAILURE);

    if (cid7 == 0){
        execl("/bin/sh", "sh", "-c", "rm -f /home/kali/modul2/darat/*bird*", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
```
* `rm` adalah perintah dalam linux untuk melakukan *remove*
* `-f` atau *force* digunakan untuk memaksa program agar dapat langsung *remove* tanpa mengeluarkan prompt
* `/home/kali/modul2/darat/*bird*` berarti menghapus seluruh file yang mengandung tulisan **bird** pada folder darat.

### Screenshot output soal 3d
Berikut adalah screenshot output dari soal 3d:

![folder darat tanpa burung](img/isi%20darat%20no%20burung.png)

## 3e
[Daftar isi](#daftar-isi)</br>
Untuk memasukkan list nama file ke dalam file `list.txt` dengan format yang diberikan (UID_UID Permission_Filename), kami menggunakan program berikut:
```c
    //input file name format: uid_uid permission_filename
    cid8 = fork();
    if(cid8 < 0) exit(EXIT_FAILURE);
  
    if (cid8 == 0){
        execl("/bin/sh", "sh", "-c", "ls -la /home/kali/modul2/air | awk 'NR > 3 {print $3\"_\"substr($1,2,2)\"_\"$9}' > /home/kali/modul2/air/list.txt", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
```
* `sh` untuk menjalankan command pada shell
* `ls -la /home/kali/modul2/air` untuk mendapatkan list detail seluruh permission dan nama user yang terdapat dalam `/home/kali/modul2/air`
* Selanjutnya untuk mengolah data pada `ls -la`, digunakan `awk` untuk melakukan print dan memasukkan ke dalamm `list.txt`.

Hasil ls -la (pada terminal):

![hasil ls -la](img/hasil%20ls%20-la.png)

* Karena pada NR 1 hingga 3 bukan merupakan file (terlihat dari kolom pertama dan huruf pertama, jika `d` merupakan directory, jika `-` merupakan file), maka proses awk dijalankan ketika `NR > 3`
* Karena FS tidak diberikan, maka secara default FS merupakan spasi/jarak antar kolom. `UID` terletak pada kolom ke-3 atau `$3`, `UID Permission` terletak pada kolom pertama atau `$1`, `Nama File` terletak pada kolom terakhir atau kolom ke-9 atau `$9`.
* Sehingga untuk mencetak seluruh list file yang ada dengan format **UID_UID Permission_Nama File** , dapat menggunakan command: `{print $3\"_\"substr($1,2,2)\"_\"$9}`.

* Penjelasan untuk `substr($1, 2, 2)`:
    * `substr` sendiri adalah salah satu command yang berfungsi untuk  memilih sebuah substring dari input. 
    * Substr memiliki syntax `substr(s, a, b)` yang berarti mengembalikan sebanyak **b** huruf dari string **s**, yang dimulai pada posisi/huruf ke-**a**.
    * `substr` pada program tersebut ditujukan untuk mengambil permission dari user yang terletak pada kolom pertama. Sesuai gambar berikut, user permission hanya berada pada huruf ke-2 hingga huruf ke-5. Lalu, user tidak memiliki permission untuk melakukan `x` atau `execute` pada seluruh file sehingga jumlah huruf yang diambil hanya 2 dan dimulai pada huruf ke-2 pula.

    ![permission](img/permission.png)
    
    Source gambar: https://www.guru99.com/file-permissions.html

* `> /home/kali/modul2/air/list.txt` digunakan untuk memasukkan hasil proses awk ke dalam file list.txt yang terletak pada directory `/home/kali/modul2/air/`.

### Screenshot output soal 3e
File list.txt pada folder air:

![list.txt di air](img/file%20list%20di%20air.png)

Isi file list.txt:

![isi list.txt](img/isi%20list.txt.png)

## Kendala Nomor 3
1. Sumber yang tersedia sedikit
2. Terdapat kesulitan saat mengerjakan soal nomor 3e (untuk memasukkan ke dalam list.txt), tetapi pada akhirnya bisa diselesaikan