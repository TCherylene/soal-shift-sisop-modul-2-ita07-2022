#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>

int main(void){
    pid_t cid1, cid2, cid3, cid4, cid5, cid6, cid7, cid8;
    int status;

    //bikin folder darat
    cid1 = fork();
    if(cid1 < 0) exit(EXIT_FAILURE);

    if(cid1 == 0){
        char *argv[] = {"mkdir", "-p", "/home/kali/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    } else 
        while((wait(&status)) > 0);
        
    //bikin folder air
    cid2 = fork();
    if(cid2 < 0) exit(EXIT_FAILURE);

    if(cid2 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/kali/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    } else while((wait(&status)) > 0);
        
    //unzip animal.zip
    cid3 = fork();

    if(cid3 < 0) exit(EXIT_FAILURE);

    if(cid3 == 0){
        char *argv[] = {"unzip", "-oq", "/home/kali/Downloads/animal.zip", "-d", "/home/kali/modul2/", NULL};
        execv("/usr/bin/unzip", argv);
    } else while((wait(&status)) > 0);
    
    //pindahin ke darat
    cid4 = fork();
    if(cid4 < 0) exit(EXIT_FAILURE);

    if(cid4 == 0){
        execl("/usr/bin/find", "find", "/home/kali/modul2/animal/", "-type", "f", "-name", "*darat*", "-exec", "mv", "-t", "/home/kali/modul2/darat", "{}", "+", (char *) NULL);
    } else while((wait(&status)) > 0);
                
    //pindahin ke air
    cid5 = fork();
    if(cid5 < 0) exit(EXIT_FAILURE);

    if(cid5 == 0){
        sleep(3);
        execl("/usr/bin/find", "find", "/home/kali/modul2/animal/", "-type", "f", "-name", "*air*", "-exec", "mv", "-t", "/home/kali/modul2/air/", "{}", "+", (char *) NULL);
    } else while(wait((&status)) > 0);
        
    //remove file in dir: animal
    cid6 = fork();

    if(cid6 < 0) exit(EXIT_FAILURE);

    if(cid6 == 0) 
        execl("/bin/sh", "sh", "-c", "rm -f /home/kali/modul2/animal/*", (char *) NULL);
    else while(wait((&status)) > 0);
        
    //remove bird
    cid7 = fork();
    if(cid7 < 0) exit(EXIT_FAILURE);

    if (cid7 == 0){
        execl("/bin/sh", "sh", "-c", "rm -f /home/kali/modul2/darat/*bird*", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);

    //input file name format: uid_uid permission_filename
    cid8 = fork();
    if(cid8 < 0) exit(EXIT_FAILURE);
  
    if (cid8 == 0){
        execl("/bin/sh", "sh", "-c", "ls -la /home/kali/modul2/air | awk 'NR > 3 {print $3\"_\"substr($1,2,2)\"_\"$9}' > /home/kali/modul2/air/list.txt", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
}